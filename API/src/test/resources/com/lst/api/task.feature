Feature: Check short link

@severity=critical
@api
Scenario: Check short link
  Given create short link
  When check created short link is exist
  And remove created short link
  Then check created link is not exist
