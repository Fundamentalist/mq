package com.lst.api.constants;

public class Constants {

    public static String PORTAL_URL ="portal.url";
    public static String PORTAL_TOKEN ="portal.token";
    public static String PORTAL_MQL5_URL ="portal.mql5.url";
}
