package com.lst.api;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static com.lst.api.LstToAPITest.config;
import static com.lst.api.constants.Constants.*;

public class LstToAPI {

    final static Logger logger = Logger.getLogger(LstToAPI.class);

    private static String shortLinkValue = "";

    public LstToAPI() {
    }

    public void createLink(){

        RestAssured.baseURI = config.getProperty(PORTAL_URL);
        RequestSpecification request = given();

        JSONObject json = new JSONObject();
        json.put("type", "link");
        json.put("url", config.getProperty(PORTAL_MQL5_URL));
        json.put("utm", "utm_campaign=[domain]");

        JSONObject data = new JSONObject();
        data.put("data", json);

        request.header("X-AUTH-TOKEN", config.getProperty(PORTAL_TOKEN));
        request.header("Content-Type", "application/json");
        request.body(data.toString());
        Response response = request.post();

        int statusCode = response.getStatusCode();
        assertEquals(200, statusCode);

        String urlData = response.jsonPath().getString("data.url");
        assertEquals(config.getProperty(PORTAL_MQL5_URL), urlData);
        String shortLink = response.jsonPath().getString("data.short");
        shortLinkValue = shortLink.substring(15);

        logger.info("Short link created successfully");
    }


    public void checkLinkIsExist(){
        RestAssured.baseURI = config.getProperty(PORTAL_URL) + "/" + shortLinkValue;
        RequestSpecification request = given();

        request.header("X-AUTH-TOKEN", config.getProperty(PORTAL_TOKEN));
        request.header("Content-Type", "application/json");
        Response response = request.get();

        int statusCode = response.getStatusCode();
        assertEquals(200, statusCode);

        logger.info("Short link is exist successfully");
    }

    public void removeLink(){
        RestAssured.baseURI = config.getProperty(PORTAL_URL) + "/" + shortLinkValue;
        RequestSpecification request = given();

        request.header("X-AUTH-TOKEN", config.getProperty(PORTAL_TOKEN));
        request.header("Content-Type", "application/json");
        Response response = request.delete();

        int statusCode = response.getStatusCode();
        assertEquals(200, statusCode);

        logger.info("Short link removed successfully");
    }

    public void checkLinkIsNotExist(){
        RestAssured.baseURI = config.getProperty(PORTAL_URL) + "/" + shortLinkValue;
        RequestSpecification request = given();

        request.header("X-AUTH-TOKEN", config.getProperty(PORTAL_TOKEN));
        request.header("Content-Type", "application/json");
        Response response = request.get();

        int statusCode = response.getStatusCode();
        assertEquals(400, statusCode);

        logger.info("Short link is not exist successfully");
    }
}
