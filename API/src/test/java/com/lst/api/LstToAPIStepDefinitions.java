package com.lst.api;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LstToAPIStepDefinitions {

    private LstToAPI lstToAPI = new LstToAPI();

    @Given("^create short link$")
    public void createLink() {
        lstToAPI.createLink();
    }

    @When("^check created short link is exist$")
    public void checkLinkIsExist() {
        lstToAPI.checkLinkIsExist();
    }

    @When("^remove created short link$")
    public void removeLink() {
        lstToAPI.removeLink();
    }

    @Then("^check created link is not exist$")
    public void checkLinkIsNotExist() {
        lstToAPI.checkLinkIsNotExist();
    }

}
