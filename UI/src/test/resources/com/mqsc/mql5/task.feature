Feature: Check economic calendar

@severity=blocker
@ui
@economicCalendar
Scenario: Check economic calendar
  Given user opens portal
  When filter calendar events by period "Current month" on economic calendar page
  And filter calendar events by importance's list on economic calendar page:
    | Medium   |
#    | Low      |
  And filter calendar events by currency's list on economic calendar page:
    | CHF - Swiss frank   |
#    | CNY - Chinese yuan  |
  And open the first economic calendar link "CHF" in the filtered list on economic calendar page:
  And event must have value from currency's list on about company page:
#    | CNY, Chinese yuan |
    | CHF, Swiss frank  |
  And event must have value from importance's list on about company page:
#    | low  |
    | medium   |
  And click on history tab on about company page
  Then log event history for the past "12" months in a table to log file
