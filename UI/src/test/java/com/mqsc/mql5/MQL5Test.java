package com.mqsc.mql5;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.TextReport;
import com.codeborne.selenide.logevents.SelenideLogger;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.qameta.allure.selenide.AllureSelenide;

import java.io.FileInputStream;
import java.util.Properties;
import java.io.IOException;

@RunWith(Cucumber.class)
public class MQL5Test {

    public static Properties config = null;

    @Rule
    public TestRule report = new TextReport().onFailedTest(true).onSucceededTest(true);

    @BeforeClass
    public static void initSettings() throws IOException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setAcceptInsecureCerts(true);
        Configuration.browserCapabilities = capabilities;
        Configuration.startMaximized = true;

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
        Configuration.reportsFolder = "target/reports";

        config = new Properties();
        FileInputStream ip = new FileInputStream(
                System.getProperty("user.dir") + "/src/test/resources/application.properties");
        config.load(ip);
    }
}
