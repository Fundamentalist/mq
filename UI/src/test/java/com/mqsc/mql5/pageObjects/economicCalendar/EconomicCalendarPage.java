package com.mqsc.mql5.pageObjects.economicCalendar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.mqsc.mql5.actions.ActionsImpl;
import com.mqsc.mql5.pageObjects.CommonElements;
import org.openqa.selenium.By;

import java.util.List;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class EconomicCalendarPage extends CommonElements {

    private final SelenideElement firstEconomicCalendarLink = $("#economicCalendarTable .ec-table__item a");

    public void choosePeriod(String periodValue){
        SelenideElement period = $$("#economicCalendarFilterDate label").findBy(text(periodValue));
        period.click();
    }

    public void chooseImportance(List<String> importances){
        List<String> importanceList = $$("#economicCalendarFilterImportance label").texts();
        importanceList.removeAll(importances);

        importanceList.forEach(importance ->
                $$("#economicCalendarFilterImportance label").findBy(text(importance)).click());
    }

    public void chooseCurrency(List<String> currencys){
        List<String> currencyList = $$("#economicCalendarFilterCurrency label").texts();

        currencyList.removeAll(currencys);

        currencyList
                .forEach(currency -> {
                    $$("#economicCalendarFilterCurrency label").findBy(text(currency)).click();
                    ActionsImpl actions = new ActionsImpl();
                    actions.scrollDown(15);
                });
    }

    public void openEconomicCalendarLink(String currency){
        $("#economicCalendarTable")
                .find(byText(currency))
                .closest(".ec-table__item")
                .find("a")
                .click();
    }
}
