package com.mqsc.mql5.pageObjects.economicCalendar;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.mqsc.mql5.pageObjects.CommonElements;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class AboutCompanyPage extends CommonElements {

    final static Logger logger = Logger.getLogger(AboutCompanyPage.class);

    private final SelenideElement currencyArea = $(".economic-calendar__event-header-currency");
    private final SelenideElement historyTab = $("[data-content=\"history\"]");

    public void checkCurrency(List<String> currencyList){
        String currencyValue = currencyList.stream()
                .filter(currency -> currencyArea.text().equals(currency))
                .findFirst()
                .orElse(null);

        assert currencyValue != null;
    }

    public void checkImportance(List<String> importanceList){
        String importanceValue = importanceList.stream()
                .filter(importance -> $(".event-table__importance." + importance).isDisplayed())
                .findFirst()
                .orElse(null);

        assert importanceValue != null;
    }

    public void clickOnHistoryTab(){
        historyTab.click();
    }

    public void getHistoryByPeriod(int months){

        logger.info("| Date | Actual | Forecast | Previous |");

        ElementsCollection historyRows = $$("#tab_content_history .event-table-history__item");

        for(SelenideElement row : historyRows ) {
            String date = row.$(".event-table-history__date").text();

            LocalDate dateNow = LocalDate.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy", Locale.ENGLISH);
            String formattedDate = dateNow.format(formatter);
            LocalDate parsedDateNow = LocalDate.parse(formattedDate, formatter);

            LocalDate expectedDate = parsedDateNow.minus(Period.ofMonths(months));

            LocalDate rowDate = LocalDate.parse(date, formatter);

            boolean result = rowDate.isAfter(expectedDate);

            if (result){
                String actual = row.$(".event-table-history__actual").text();
                String forecast = row.$(".event-table-history__forecast").text();
                String previous = row.$(".event-table-history__previous").text();
                logger.info("| " + date + " | " + actual + " | " + forecast + " | " + previous);
            }
        }
    }
}
