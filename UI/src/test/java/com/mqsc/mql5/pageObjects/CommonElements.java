package com.mqsc.mql5.pageObjects;

import static com.codeborne.selenide.Selenide.open;
import static com.mqsc.mql5.MQL5Test.config;
import static com.mqsc.mql5.constants.Constants.PORTAL_URL;

public class CommonElements {

    public void openDefaultSite(){
        String portalURL = config.getProperty(PORTAL_URL);
        open(portalURL);
    }
}
