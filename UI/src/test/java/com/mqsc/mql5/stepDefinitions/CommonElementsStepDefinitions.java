package com.mqsc.mql5.stepDefinitions;

import com.mqsc.mql5.config.MQL5Context;
import cucumber.api.java.en.Given;

public class CommonElementsStepDefinitions implements MQL5Context {

    @Given("^user opens portal$")
    public void openDefaultSite() {
        COMMON_ELEMENTS.openDefaultSite();
    }

}
