package com.mqsc.mql5.stepDefinitions.economicCalendar;

import com.mqsc.mql5.config.MQL5Context;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

public class AboutCompanyStepDefinitions implements MQL5Context {

    @When("^event must have value from currency's list on about company page:$")
    public void checkCurrency(List<String> currencyList) {
        ABOUT_COMPANY_PAGE.checkCurrency(currencyList);
    }

    @When("^event must have value from importance's list on about company page:$")
    public void checkImportance(List<String> importanceList) {
        ABOUT_COMPANY_PAGE.checkImportance(importanceList);
    }

    @When("^click on history tab on about company page$")
    public void clickOnHistoryTab() {
        ABOUT_COMPANY_PAGE.clickOnHistoryTab();
    }

    @Then("^log event history for the past \"([^\"]*)\" months in a table to log file$")
    public void getHistoryByPeriod(int months) {
        ABOUT_COMPANY_PAGE.getHistoryByPeriod(months);
    }
}
