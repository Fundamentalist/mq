package com.mqsc.mql5.stepDefinitions.economicCalendar;

import com.mqsc.mql5.config.MQL5Context;
import cucumber.api.java.en.When;

import java.util.List;

public class EconomicCalendarStepDefinitions implements MQL5Context {

    @When("^filter calendar events by period \"([^\"]*)\" on economic calendar page$")
    public void choosePeriod(String period) {
        ECONOMIC_CALENDAR_PAGE.choosePeriod(period);
    }

    @When("^open the first economic calendar link \"([^\"]*)\" in the filtered list on economic calendar page:$")
    public void openEconomicCalendarLink(String currency) {
        ECONOMIC_CALENDAR_PAGE.openEconomicCalendarLink(currency);
    }

    @When("^filter calendar events by importance's list on economic calendar page:$")
    public void chooseImportance(List<String> importanceList) {
        ECONOMIC_CALENDAR_PAGE.chooseImportance(importanceList);
    }

    @When("^filter calendar events by currency's list on economic calendar page:$")
    public void chooseCurrency(List<String> currencyList) {
        ECONOMIC_CALENDAR_PAGE.chooseCurrency(currencyList);
    }

}
