package com.mqsc.mql5.config;

import com.mqsc.mql5.pageObjects.CommonElements;
import com.mqsc.mql5.pageObjects.economicCalendar.AboutCompanyPage;
import com.mqsc.mql5.pageObjects.economicCalendar.EconomicCalendarPage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public interface MQL5Context {

    ApplicationContext context =
            new FileSystemXmlApplicationContext("classpath:message-bean.xml");

    EconomicCalendarPage ECONOMIC_CALENDAR_PAGE = (EconomicCalendarPage) context.getBean("economicCalendarPage");

    AboutCompanyPage ABOUT_COMPANY_PAGE = (AboutCompanyPage) context.getBean("aboutCompanyPage");

    CommonElements COMMON_ELEMENTS = (CommonElements) context.getBean("commonElements");

}
