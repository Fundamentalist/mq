package com.mqsc.mql5.actions;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

public class ActionsImpl {

    public void scrollDown(int pxValue) {
        Selenide.executeJavaScript("window.scrollBy(0," + pxValue + ")", "");
    }

    public void scrollUp(int pxValue) {
        Selenide.executeJavaScript("window.scrollBy(0,-" + pxValue + ")", "");
    }

    public void moveToElement(SelenideElement selenideElement) {
        Selenide.actions().moveToElement(selenideElement).perform();
    }
}
